import sqlalchemy as sq
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql.base import InPlaceGenerative
from sqlalchemy.sql.expression import column

from content import STATUS

Base = declarative_base()

class STATUS :
    ACTIVE = '1'
    INACTIVE = '0'

class Content_core(Base):
    __tablename__ = 'Content_core'

    id_content_core = sq.column('id_content_core', sq.integer, primary_key=True)
    channel_code = sq.column('channel_code', sq.text)
    column_code = sq.column('column_code', sq.String(50))
    image_path = sq.column('image_path', sq.text)
    path_trailer = sq.column('path_trailer', sq.text)
    path_video = sq.column('path_video', sq.text)
    anevia_path_video = sq.column('anevia_path_video', sq.text)
    status = sq.column('status', sq.Enum(STATUS.ACTIVE, STATUS.INACTIVE))
    episode = sq.column('episode', sq.Integer)
    Content_id_content = ('Content_id_content', sq.Integer)
    title = sq.column('title', sq.String(45))
    Season_id_season = sq.column('Season_id_season', sq.Integer)
    isTvod = sq.column('isTvod', sq.tinyint(1))
    epg_code_slcs = sq.column('epg_code_slcs', sq.text)
    startdate = sq.column('startdate', sq.date)
    enddate = sq.column('enddate', sq.date)
    isDRM = sq.column('isDRM', sq.tinyint(1))
    created = sq.column('created', sq.DateTime)
    modified = sq.column('modified', sq.DateTime)
    created_by = sq.column('created_by', sq.Integer)
    modified_by = sq.column('modified_by', sq.Integer)
