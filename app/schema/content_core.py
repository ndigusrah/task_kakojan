from datetime import date, datetime
from typing import Optional
from pydantic import BaseModel
from sqlalchemy.sql.sqltypes import Enum
from enum import IntEnum

class Status(IntEnum):
    yes = 1
    no = 0

class Content_coreSchema(BaseModel):
    channel_code: str = None
    column_code: str = None
    image_path: str = None
    path_trailer: str = None
    path_video: str = None
    anevia_path_video: str = None
    status: Status = Status.no
    episode: int = None
    Content_id_content: int = None
    title: str 
    Season_id_season: int = None
    isTvod: int = None
    epg_code_slcs = int = None
    stardate : date = None
    enddate: date = None
    isDRM: int = None
    created : datetime = None
    modified: datetime =  None
    created_by: int = None
    modified_by: int = None

class Content_coreUpdateSchema(BaseModel):

 channel_code:Optional [str]
 column_code:Optional [str]
 image_path:Optional[str]
 path_trailer:Optional[str]
 path_video:Optional[str]
 status:Optional[Status]
 episode:Optional[int]
 Content_id_content:Optional[int]
 title:Optional[str]
 Season_id_season:Optional[int]
 isTvod:Optional[int]
 epg_code_slcs:Optional[str]
 stardate:Optional[date]
 enddate:Optional[date]
 isDRM:Optional[int]
 created:Optional[datetime]
 modified:Optional[datetime]
 created_by:Optional[int]
 modified_by:Optional[int]

