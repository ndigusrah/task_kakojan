import sqlalchemy as sa
from sqlalchemy.orm import declarative_base

Base = declarative_base()

class STATUS:
    ACTIVE = 'Active'
    INACTIVE = 'Inactive'

class TYPE:
    MOVIES = 'Movies'
    CHANNEL = 'Channel'
    SERIES = 'Series'
    CLIPS = 'Clips'
    CLIPSPLAYLIST = 'ClipsPlaylist'

class IS_RECOMMENDED:
    YES = 'Yes'
    NO = 'No'

class IS_GEOBLOCK:
    NO = 'No'
    BLACKLIST = 'Blacklist'
    WHITELIST = 'Whitelist'

class IS_FREE:
    YES = 'Yes'
    NO = 'No'

class IS_ORIGINAL:
    YES = 'Yes'
    NO = 'No'

class IS_BRANDED:
    YES = 'Yes'
    NO = 'No'

class IS_EXCLUSIVE:
    YES = 'Yes'
    NO = 'No'

class RATING:
    SU = 'SU'
    A = 'A'
    BOA = 'BO-A'
    BO = 'BO'
    BOSU = 'BO-SU'
    R = 'R'
    D = 'D'

class SHOW_VIEWS_COUNT:
    YES = 'Yes'
    NO = 'No'

class SHOW_LIKES_COUNT:
    YES = 'Yes'
    NO = 'No'


class Content(Base):
    __tablename__ = 'Content'

    id_content = sa.Column('id_content', sa.Integer, primary_key=True)
    title = sa.Column('title', sa.String(45))
    sorting = sa.Column('sorting', sa.Integer)
    synopsis = sa.Column('synopsis', sa.Text)
    synopsis_en = sa.Column('synopsis_en', sa.Text)
    year = sa.Column('year', sa.String(4))
    big_poster = sa.Column('big_poster', sa.Text)
    thumb_poster = sa.Column('thumb_poster', sa.Text)
    banner_landscape = sa.Column('banner_landscape', sa.Text)
    type = sa.Column('type', sa.Enum(TYPE.MOVIES, TYPE.CHANNEL, TYPE.SERIES, TYPE.CLIPSPLAYLIST, TYPE.CLIPS))
    status = sa.Column('status', sa.Enum(STATUS.ACTIVE, STATUS.INACTIVE))
    start_date = sa.Column('start_date', sa.DateTime)
    end_date = sa.Column('end_date', sa.DateTime)
    iom = sa.Column('iom', sa.Text)
    created = sa.Column('created', sa.DateTime)
    modified = sa.Column('modified', sa.DateTime)
    created_by = sa.Column('created_by', sa.Integer)
    modified_by = sa.Column('modified_by', sa.Integer)
    is_recommended = sa.Column('isRecommended', sa.Enum(IS_RECOMMENDED.YES, IS_RECOMMENDED.NO))
    is_geoblock = sa.Column('isGeoblock', sa.Enum(IS_GEOBLOCK.NO, IS_GEOBLOCK.BLACKLIST, IS_GEOBLOCK.WHITELIST))
    id_frontend_navigation = sa.Column('id_frontend_navigation', sa.Integer)
    tags = sa.Column('tags', sa.String(45))
    id_role = sa.Column('id_role', sa.String(20))
    category_id = sa.Column('category_id', sa.Integer)
    subcategory_id = sa.Column('subcategory_id', sa.Integer)
    is_free = sa.Column('isFree', sa.Enum(IS_FREE.YES, IS_FREE.NO))
    is_original = sa.Column('isOriginal', sa.Enum(IS_ORIGINAL.YES, IS_ORIGINAL.NO))
    is_branded = sa.Column('isBranded', sa.Enum(IS_BRANDED.YES, IS_BRANDED.NO))
    is_exclusive = sa.Column('isExclusive', sa.Enum(IS_EXCLUSIVE.YES, IS_EXCLUSIVE.NO))
    rating = sa.Column('rating', sa.Enum(RATING.SU, RATING.A, RATING.BOA, RATING.BO, RATING.BOSU, RATING.R, RATING.D))
    duration_minute = sa.Column('duration_minute', sa.Integer)
    show_views_count = sa.Column('show_views_count', sa.Enum(SHOW_VIEWS_COUNT.YES, SHOW_VIEWS_COUNT.NO))
    show_likes_count = sa.Column('show_likes_count', sa.Enum(SHOW_LIKES_COUNT.YES, SHOW_LIKES_COUNT.NO))
    url_square_poster = sa.Column('url_square_poster', sa.Text)
    url_web_banner_detail = sa.Column('url_web_banner_detail', sa.Text)
    url_portrait_poster = sa.Column('url_portrait_poster', sa.Text)
    url_wide_poster = sa.Column('url_wide_poster', sa.Text)
    additional = sa.Column('additional', sa.JSON)
    